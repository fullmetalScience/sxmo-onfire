# codes - Card-balance Manager

**codes** was created to monitor the spending from various types of pre-paid accounts.

Its primary purpose is to keep track of balances on gift cards and display their corresponding barcodes for scanning by merchants.

Values are stored without units, effectively rendering the tool currency-agnostic.


## Installation

1. Get **codes** by cloning [sxmo-onfire](../).
2. Install dependencies listed by this command:
        _common/assert_environment.sh codes/codes.sh
3. *[if using Sxmo]* Activate the tool by linking it into userscripts:
        ln -s ~/sxmo-onfire/codes/codes.sh ~/.config/sxmo/userscripts/


## Usage

1. Run `./codes.sh` (or select "Codes" from Sxmo's scripts-menu)
2. Enter a new card in the format `<merchant>-<code>` and press Shift+Enter.
3. Enter the initial value of the card and press Shift+Enter.
4. Whenever you spend from a card, select it and log the amount spent.


## Features

* If an image named `<merchant>-<code>.png` is found in `$XDG_DATA_HOME/codes/`, the Spend-menu offers to display it. Use `-` and `=` to zoom in and out.
* If [barcode_gen](https://github.com/revilo/barcode_gen) is available and the merchant is known (hard-coded), the image is generated automatically.
* Spends are written to a CSV file that can be processed by [hledger](https://hledger.org) using the included rules file. Example: `hledger print -f codes.csv --rules-file='codes.csv.rules'`


## Roadmap

* shorten codes for display to fit into portrait-mode (until implemented, you may want to save screen-space by adding only the last digits of a code - e.g. `22881` instead of `XYZ999300022881`; note that this breaks automatic barcode-generation)
* support comments in spends: `22.35 # silver`
* support equal-sign in spends to set to a new balance: `=20`
* support entering expiration dates for codes
* order cards by expiration dates
* offer previously used amounts for top-up or fall back to a ladder of default ones
* if barcode format for a merchant is unknown, offer common types to generate, storing files with suffixes (supported: `EAN13`, `EAN8`, `CODE39`, `CODE93`, `CODE11`, `CODE128`, `CODABAR`, `ITF`, `STF`)
* consider image files whose tags are never mentioned in the CSV as new and show a tag (with zero balance) for top up; extend Usage#2 to reflect the change
