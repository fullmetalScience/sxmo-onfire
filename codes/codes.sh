#!/bin/sh
# title="${icon_wn2:-} Codes"
#
# TOOLS bemenu|dmenu bc sed cut column(alpine:util-linux-misc,debian:bsdextrautils)
# TOOLS [swayimg] [barcode_gen]

# shellcheck source=/_common/init.sh
REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init.sh"
# shellcheck source=/_common/init_gui.sh
REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init_gui.sh"

SHOWCODE="${icon_img:-🪧} Show code"
CANCEL="${icon_cls:-❌} Cancel"
DPROMPT="${icon_wn2:-} "

# DATA is initialized by init.sh
# NOTE: On 2024-04-22 this path was changed from .../codes/ to .../sxmo-onfire/codes/
#       Please move your existing directory accordingly if your data was created before this date.
CSV="${DATA}/codes.csv"

write () {
        tag=$1 && shift
        amount=$1 && shift
        balance=$1 && shift

        code=0
        if printf "%.2f %.2f" "$amount" "$balance" 2>/dev/null 1>&2 ; then
                # For future use - "expiry" could be provided by user at Top-Up
                expiry=$(date +%Y-%m-%d)
                # TODO write correct expiry by fetching it from last entry of the same card
                echo "$expiry,$(date +%Y-%m-%d),${tag%-*},${tag##*-},$amount,$balance," >> "$CSV"
        else
                code=$?
                echo "Error: Amount '$amount' seems to be invalid."
        fi

        return "$code"
}

top_up () {
        tag=$1 && shift
        amount=$1 && shift
        balance=$1 && shift

        write "$tag" "$(echo "$amount" | tr -d '-')" "$balance"
}

spend () {
        tag=$1 && shift
        amount=$1 && shift
        balance=$1 && shift

        write "$tag" "$(echo "$amount" | tr -d '-' | sed 's|^|-|g')" "$balance"
}

balance_of () {
        tag=$1 && shift

        # The "grep" below is equal to:
        #hledger balance --empty -f "$JOURNAL" "tag:$tag" | tail -n1 | awk '{ print $1 }'

        BALANCE_FIELD=6
        grep -F "$tag" "$CSV" | tail -n1 | cut -d, -f "$BALANCE_FIELD"
}

BC () {
        expression=$1 && shift
        [ 0 -eq "$(echo "$expression" | bc)" ] && code=1 || code=0

        return "$code"
}

show_code () {
        if [ -s "$DATA/$tag.png" ] ; then
                echo "Opening $DATA/$tag.png ..." >> "$DEBUG"
                if type swayimg > /dev/null ; then
                        swayimg -s real -inw "${ONFIRE_COLOR1:-069}" "$DATA/$tag.png"
                else
                        xdg-open "$DATA/$tag.png"
                fi
        else
                echo "No code found at $DATA/$tag.png" >> "$DEBUG"
        fi
}

get_amount () {
        tag=$1 && shift
        balance=$1 && shift
        balance=${balance:-0}

        if [ ! -e "$DATA/$tag.png" ] ; then
                echo "No $DATA/$tag.png found. Attempting to generate a code instead ..." >> "$DEBUG"
                genarate_code_for "$tag"
        fi

        prompt='Spend' ; [ "$balance" = '0' ] && prompt='Top-up'
        ENTRIES="$CANCEL" ; [ -s "$DATA/$tag.png" ] && ENTRIES="$SHOWCODE\n$CANCEL"

        amount=
        unset PICKED && while [ -z "$PICKED" ] || [ "$PICKED" = "$SHOWCODE" ] ; do
                PICKED=$(printf %b "$ENTRIES" | $MENU_WITH_KB -i -p "$DPROMPT [$balance] $prompt:")

                case "$PICKED" in
                        "$SHOWCODE")  show_code ;;
                        "$CANCEL"|'') amount=0  ;;
                        *)
                                amount="$PICKED"
                                if BC "$balance != 0" ; then
                                        BC "$PICKED <= 0 || $PICKED > ${balance%%-*}" &&
                                                unset PICKED
                                fi
                                ;;
                esac
        done

        echo "$amount"
}

get_tags () {
        # sed --silent 's|^[^,]\+,[^,]\+,\(.*,.*\),[^,]*,[^,]*,.*|\1|p' "$CSV" | sort | uniq
        cut -d, -f3-4 "$CSV" | sort | uniq
}

get_taglist () {
        taglist=
        for t in $(get_tags) ; do
                #echo "Checking balance of $t ..." >> "$DEBUG"
                available=$(balance_of "$t")
                echo "Tag $t has balance '$available'" >> "$DEBUG"
                if [ "$available" != 0 ] ; then
                        tag=$(echo "$t" | sed 's|,|-|g')
                        if [ -z "$taglist" ] ; then
                                taglist="$tag $available"
                        else
                                taglist="$taglist\n$tag $available"
                        fi
                fi
        done

        echo "$taglist"
}

select_card () {
        ENTRIES=$(get_taglist | column -ts' ' -o'    ')

        PICKED=$(printf %b "$ENTRIES\n$CANCEL" | $MENU_WITH_KB -i -p "${icon_lgt:-$DPROMPT} Card:")

        case "$PICKED" in
                "$CANCEL") ;;
                *)         echo "$PICKED" ;;
        esac
}

genarate_code_for () {
        tag=$1 && shift

        if type barcode_gen > /dev/null ; then
                code_type=
                case "${tag%-*}" in
                        cepsa)  code_type='CODE128' ;;
                        *)      echo "WARNING: Generating barcodes for ${tag%-*} cards not yet supported." >> "$DEBUG" ;;
                esac

                if [ -n "$code_type" ] ; then
                        card="${tag##*-}" # e.g. AML1000011489538
                        barcode_gen --type "$code_type" "À$card" --file "$DATA/$tag.png"
                fi
        else
                echo "WARNING: barcode_gen not available." >> "$DEBUG"
        fi
}

main () {
        set -e
        [ ! -f "$CSV" ] && mkdir -p "$(dirname "$CSV")" && touch "$CSV"

        tag_and_balance="$(select_card) "
        while [ "$tag_and_balance" != ' ' ] ; do
                tag=$(echo "$tag_and_balance" | awk '{ print $1 }')
                balance=$(echo "$tag_and_balance" | awk '{ print $2 }')
                
                echo "TAG: $tag BALANCE: $balance" >> "$DEBUG"
                
                amount=$(get_amount "$tag" "$balance")

                echo "AMOUNT: $amount" >> "$DEBUG"
                
                if [ -n "$amount" ] ; then
                        if BC "$amount > 0" ; then
                                [ -z "$balance" ] && balance=0

                                if BC "$balance == 0" ; then
                                        BC "$amount > 0" &&
                                                top_up "$tag" "$amount" "$(echo "scale=12; $balance+$amount" | bc)"
                                else
                                        spend "$tag" "$amount" "$(echo "scale=12; $balance-$amount" | bc)"
                                fi
                        fi
                fi

                tag_and_balance="$(select_card) "
        done
}

main
