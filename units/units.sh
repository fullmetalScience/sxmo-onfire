#!/bin/sh
# title="$icon_clc Calculator"
# include common definitions
#
# TOOLS bemenu|dmenu units|gunits(brew:gnu-units) units-cur-update|units_cur|gunits_cur(brew:gnu-units) xclip|xsel|wl-copy|pbcopy curl jq

set -u

# shellcheck source=/_common/init.sh
REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init.sh"
# shellcheck source=/_common/init_gui.sh
REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init_gui.sh"


SYMBOL="${icon_clc:-}"
CANCEL="${icon_cls:-x} Cancel"
COPY="${icon_cpy:-} "
UPDATE_RATES="${icon_try:-🔄} Update currencies"

CLIP=xclip ; type xsel >/dev/null && CLIP=xsel ; type wl-copy >/dev/null && CLIP=wl-copy ; type pbcopy >/dev/null && CLIP=pbcopy
UNITS=units ; type gunits >/dev/null && UNITS=gunits

# Both Bitfinex and Kraken cloudflare the response when Tor is used
#readonly DEFAULT_TOR_PROXY='127.0.0.1:9050'
#PROXY="${PROXY:-$DEFAULT_TOR_PROXY}"

CRYPTO_FILE="${CACHE}/bitfinex.units"
CURRENCY_FILE="${CACHE}/currency.units"
mkdir -p "$CACHE" || echo "Error creating cache directory at $CACHE." >&2


this_script_clashes_with () {
        candidate=$1 && shift

        tcode=1
        if type realpath >/dev/null ; then
                units_executable=$(realpath "$(command -v "$candidate")")
                this_script=$(realpath "$0")
                [ "$units_executable" = "$this_script" ] &&
                        tcode=0
        fi

        return $tcode
}


is_likely_a_currency () {
        candidate=$1 && shift

        likely_a_currency=no
        if [ -n "$candidate" ] ; then

                if echo "$candidate" | grep -qwE 'XMR|monero|ɱ' ; then
                        likely_a_currency=yes
                else
                        grep -qw "$candidate" /usr/share/units/currency.units &&
                                likely_a_currency=yes
                fi
        else
                likely_a_currency=no
                echo "\$candidate is empty." >> "$DEBUG"
        fi

        icode=1 ; [ "$likely_a_currency" = 'yes' ] && icode=0

        echo "${candidate##* }"
        return "$icode"
}


UNITS () {
        for f in "$CRYPTO_FILE" "$CURRENCY_FILE" ; do
                if [ -s "$f" ] ; then
                        echo "File $f seems to be fine." >> "$DEBUG"
                else
                        echo "Touching unit file $f with date of epoch ..." >> "$DEBUG"
                        touch -d@0 "$f"
                fi
        done

        $UNITS -f '' -f "$CURRENCY_FILE" -f "$CRYPTO_FILE" "$@"
}

compute () {
        x=$1 && shift
        y=${1:-}

        error=0
        # Not bulletproof, but will do for a start
        userinput_without_value=$(echo "$x" | tr -d '[:digit:][:punct:][:space:]')
        echo "x: $x" >> "$DEBUG"
        echo "userinput_without_value: $userinput_without_value" >> "$DEBUG"
        to=
        # Example errors may be triggered via input 'a+b' or '='
        case "$userinput_without_value" in
                '')     result=$(UNITS --terse "$x") || error=1 ;;
                *)
                        if conformable=$(UNITS --conformable "$x") ; then
                                to=$y ; [ -z "$to" ] &&
                                        to=$(printf %s "$conformable" | $MENU_WITH_KB -i -p "${icon_lgt:-$SYMBOL} Convert to:")
                                result=$(UNITS --terse "$x" "${to%% *}")
                        else
                                error=2
                                result="$conformable"
                        fi
                        ;;
        esac

        printf '%s\n' "$result ${to%% *}"
        return "$error"
}

# TODO consider using Kraken as fallback when Bitfinex request fails
#fetch_rate_from_kraken () { # response is roughly ~317 bytes
#        curl --request GET --url 'https://api.kraken.com/0/public/Ticker?pair=XMRUSD' --header 'accept: application/json' | jq -r '.result.XXMRZUSD.c[0]'
#}

fetch_rate_from_bitfinex () { # response is roughly ~157 bytes
        fcode=0

        if response=$(curl --request GET --url 'https://api.bitfinex.com/v1/pubticker/xmrusd' --header 'accept: application/json') ; then
                if rate=$(echo "$response" | jq -r '.mid') ; then
                        echo "$rate"
                else
                        echo "Error processing this response with jq:" >&2
                        echo "$response" >&2
                fi
        else
                fcode=$?
                echo "Value of \$response: $response" >&2
        fi

        return "$fcode"
}

update_crypto_rates () {
        echo 'Updating crypto rates ...'
        ucode=0

        if rate=$(fetch_rate_from_bitfinex) ; then
                if one_usd_in_xmr=$(units -f /dev/null --terse "1 / $rate") ; then
                        cat > "$CRYPTO_FILE" << EOF
XMR      monero
ɱ        monero
!message Monero exchange rates from Bitfinex (USD base) on $(date "+%Y-%m-%d %H:%M")
monero   1|$one_usd_in_xmr    USD
EOF
                else
                        ucode=$?
                        echo "Value of \$one_usd_in_xmr: $one_usd_in_xmr" >&2
                fi
        else
                ucode=$?
                echo "Value of \$rate: $rate" >&2
        fi

        return $ucode
}

update_fiat_rates () {
        echo 'Updating fiat rates ...'
        CPI_FILE=/dev/null # unused, but written by units_cur call

        if units_cur -h | grep -iqFw cpi_file ; then
                units_cur "$CURRENCY_FILE" "$CPI_FILE"
        else
                units_cur "$CURRENCY_FILE"
        fi
}

update_units_file () {
        filename=$1 && shift

        case $filename in
                "$CRYPTO_FILE") update_crypto_rates ;;
                *)              update_fiat_rates ;;
        esac
}

determine_file_age () {
        units_file=$1 && shift

        dcode=0
        time_of_last_modification=0
        if [ -s "$units_file" ] ; then
                time_of_last_modification=$(stat --printf="%Y" "$units_file")
        else
                dcode=$?
                echo "$units_file empty or missing" >> "$DEBUG"
        fi
        time_now=$(date +%s)

        echo "$((time_now - time_of_last_modification))"
        return $dcode
}

update_file_if_older_than () {
        max_age_in_seconds=$1 && shift
        units_file=$1 && shift

        if [ "$(determine_file_age "$units_file")" -ge "$max_age_in_seconds" ] ; then
                if update_units_file "$units_file" ; then
                        echo "File $units_file updated" >> "$DEBUG"
                else
                        echo "Error updating $units_file units file!" >&2
                fi
        else
                echo "Units file $units_file is younger than $max_age_in_seconds seconds. Skipping update." >> "$DEBUG"
        fi
}

update_rates () {
        echo "Updating currency rates ..." >> "$DEBUG"

        # Busybox sh would not be able to kill the $MENU
        if type file >/dev/null && file -b /bin/sh | grep -qF 'busybox' ; then
                :
        else
                (while printf '' | $MENU -i -p "Updating currency rates ..." >/dev/null ; do continue ; done) &
                        pid="$!"
        fi

        NOW=0
        (update_file_if_older_than $NOW "$CRYPTO_FILE")& cpid=$!

        ONE_DAY=86400
        (update_file_if_older_than $ONE_DAY "$CURRENCY_FILE")& fpid=$!

        wait $cpid $fpid
        [ -n "$pid" ] && pkill -P "$pid"
}


populate_entries () {
        request=$1 && shift
        echo "Request: $request" >> "$DEBUG"

        ENTRIES=

        # TODO compute clipboard content if it's a single line and
        # tr -d '[:digit:][:punct:][:space:]' leaves a valid-looking unit
        # (grep against currency.units definitions.units elements.units bitfinex.units)
        pcode=0
        if [ -n "$request" ] && [ "$request" != ' ' ] ; then
                if result=$(compute "${request}") ; then
                        ENTRIES="$COPY $result\n$COPY ${request} = $result\n"
                else
                        pcode=$?
                        request=
                        ENTRIES="${icon_cls:-x} $result\n"
                fi
        else
                echo 'Request is empty' >> "$DEBUG"
        fi

        age_hint=
        if age=$(determine_file_age "$CRYPTO_FILE") ; then
                [ "$age" -ge "$UPDATE_THRESHOLD" ] && age_hint="($((age / 60)) minutes old)"
        else
                age_hint='(uninitialized)'
        fi
        ENTRIES="$ENTRIES\n$UPDATE_RATES $age_hint\n$CANCEL"

        echo "$ENTRIES"
        return $pcode
}

clip_and_echo () {
        str=$1 && shift

        printf %s "$str" | $CLIP 
        echo "$str"
}

main () {
        code=0
        PICKED=' '

        while [ -n "$PICKED" ] ; do
                ENTRIES=$(populate_entries "$PICKED")

                PICKED=$(printf %b "$ENTRIES" | $MENU_WITH_KB -i -p "${icon_lgt:-$SYMBOL} Compute:")
                #echo "PICKED: $PICKED" >> "$DEBUG"

                case "$PICKED" in
                        *"$UPDATE_RATES"*) update_rates ; PICKED=' ' ;;
                        *"$CANCEL")        PICKED= ;;
                        *"$COPY"*)         clip_and_echo "${PICKED##"$COPY" }" ; PICKED= ;;
                esac
                
        done

        return $code
}


#DEBUG=/dev/stderr

x=${1:-}
y=${2:-}

if this_script_clashes_with 'units' ; then
        echo "The name of this script clashes with the units executable." >&2
        echo "Please rename it or remove it from \$PATH." >&2
else
        # Update on start if units file older than threshold
        ONE_HOUR=3600
        UPDATE_THRESHOLD="${UPDATE_THRESHOLD:-$ONE_HOUR}"
        if [ -z "$x" ] || unit=$(is_likely_a_currency "$x") ; then
                echo "\$x empty or${unit:- } likely a currency. Updating units file ..." >> "$DEBUG"
                if age=$(determine_file_age "$CRYPTO_FILE") ; then
                        [ "$UPDATE_THRESHOLD" -ge 0 ] && [ "$age" -ge "$UPDATE_THRESHOLD" ] &&
                                update_rates
                fi
        else
                echo "\$x not set or '${unit:-}' doesn't seem to be a currency. Skipping units file update." >> "$DEBUG"
        fi
        
        if [ -n "$x" ]; then
                result=$(compute "$x" "$y")
                clip_and_echo "$result"
        else
        	main
        fi
fi
