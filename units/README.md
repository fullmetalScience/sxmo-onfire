# units - Unit-Aware Calculator

**units** is front-end for the powerful calculator and conversion tool [GNU Units](https://www.gnu.org/software/units/).

![units](./units.webp 'Computation and conversion examples')

It supports a wide range of units - from physics to currency.

When converting, it offers only compatible units to convert to.


## Installation

1. Get **units** by cloning [sxmo-onfire](../).
2. Install dependencies listed by this command:
        _common/assert_environment.sh units/units.sh
3. *[if using Sxmo]* Activate the tool by linking it into userscripts:
        ln -s ~/sxmo-onfire/units/units.sh ~/.config/sxmo/userscripts/


## Usage

Run `./units.sh` and enter the equation you intend to compute.

If you provide arguments, the tool will run non- or semi-interactively:

* Solving an equation by providing it as the first argument:
        # non-interactive computation
        ./units.sh '6*7'
* Providing a unit with the first and another as second argument:
        # non-interactive conversion
        ./units.sh '6 feet' 'meter'
* Providing a single argument with a unit in order to select from a list of conformable ones:
        # semi-interactive conversion
        ./units.sh '20 liter'


## Features

* Results are copied to clipboard, except for interactive mode where there is a menu option for doing so.
* Monero (XMR) exchange rates are updated if the cached rates are older than the age (in seconds) provided in the `UPDATE_THRESHOLD` environment variable.
* Initial updates can be disabled by setting a negative threshold (`UPDATE_THRESHOLD=-1 ./units.sh`).


## Roadmap

* allow to start with a set of conformables 
* allow to start with pre-defined computation
* display result of current clipboard content if it's a valid equation
* offer history of recent calculations (see `~/.units_history`)
* add cron-mode for updating currency rates
