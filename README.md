# sxmo-onfire

_sxmo-onfire_ is a collection of portable productivity tools, focussed on
complementing efficient workflows.

The suite is intended for use on desktops and the simple mobile interface
[Sxmo](https://sxmo.org).

Most tools are interacted with through [*bemenu*](https://github.com/Cloudef/bemenu),
graphically or in a terminal, with `BEMENU_BACKEND=curses` enforcing use
of the latter.


**Get it through Git:**

    $ git clone https://gitlab.com/fullmetalScience/sxmo-onfire.git

[source](https://gitlab.com/fullmetalScience/sxmo-onfire),
[issues](https://gitlab.com/fullmetalScience/sxmo-onfire/-/issues)


## Hints

Particularly on desktop machines, scaling the menus may make them more pleasant to work with:

    BEMENU_SCALE=2 SXMO_DMENU_OPTS='--fn Mono 20 --tf #00ffff' ~/xmr.zone/check/check.sh
