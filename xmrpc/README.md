# XMRPC - Monero RPC Control

**XMRPC** simplifies communication with the Monero RPC. It is the back-end to [*xmrid*](../../xmr-id/) and the upcoming bemenu-based *xmrtx*.

The tool provides commands to handle Monero-related tasks, most of which translate to the instant execution of an RPC method.

_Note that methods are called without asking for confirmations. For example, you will not be asked to verify details when issuing a transfer. Be careful and implement your own critical checks accordingly._



## Installation

1. Get **XMRPC** by cloning [sxmo-onfire](../).
2. Install dependencies listed by this command:
        _common/assert_environment.sh xmrpc/xmrpc.sh
3. Install and configure Tor (see below).



## Usage

Refer to the source code or the interactive menu (see below) to find a valid command and issue `./xmrpc.sh <command> [<params>...]` to call it. Example:  `./xmrpc.sh address`

There is also a generic command named `request` (shorthand `r`), that allows you to call [*any function supported by the Monero RPC*](https://www.getmonero.org/resources/developer-guides/wallet-rpc.html). Examples:

* `r refresh ".start_height":3140000`
* `request tag_accounts "tag":"noshore","accounts":[0]`
* `r get_account_tags`


### Customization

**XMRPC** tries to adapt to your environment as best as it can. Setting environment variables restricts this behaviour in the respective areas. Example:

    NET=mainnet NODE=127.0.0.1:18081 \
        MONERO_PASSFILE=~/.password-store/mainnet-saberhagen.gpg \
        MONERO_FILE=~/mainnet-saberhagen \
        COLUMNS=$COLUMNS ./xmrtx.sh <command>


* By default, the stagenet network is connected to. `NET=mainnet` switches to mainnet.
* Automatic discovery of proxy and node can significantly delay commands related to RPC-startup. Explicitly setting `NODE` skips the guesswork and restricts use to the one provided.
* If the `MONERO_PASSFILE` name ends in `.gpg`, decryption is attempted. Otherwise it is considered a plain-text file. In either case, *only the first line* of the provided file is considered the password.
* Alternatively, a password may be specified via `MONERO_PASSWORD`. However, your shell may store the call in history, rendering this option less secure.



### Interactive mode

**XMRPC** is mainly intended to be used as back-end by other tools. When started without arguments (`./xmrpc.sh`) it presents an interactive menu that lists the most common commands:

    XMRPC 0.2.0-beta by fullmetalScience
    
        WARNING: All commands execute without asking confirmation!
    
        environment                Creates and starts everything necessary for RPC functionality
        stop [await]               Gracefully stops the RPC, optionally blocking until gone
        check_proxy <host:port>    Checks proxy availability [non-RPC]
        check_node <host:port>     Checks node availability [non-RPC]
        height                     Shows current block height
        address                    Shows our primary Monero account number
        addresses                  Shows all our Monero account numbers
        balance                    Shows available balance
        incoming [index]           Shows transfers, optionally filtered by subaddress index
        transfer <dest> <amount>   Transfers amount to destination (INSTANT!)
        validate_address <str>     Verifies a string as Monero destination
        help                       Lists documented functions [non-RPC]
        quit                       Exits the program [non-RPC]
    
    [--] Execute: _


#### Hints

* If the RPC is running, the **status-flags** to the left of the *Execute*-prompt are set. The first will be one of `M|S|T`, indicating the Monero network used (*mainnet, stagenet or testnet*). The second will be one of `I|T|C`, indicating the host type of the node (*i2p, Tor or clearnet*).
* To get **history-functionality**, install *rlwrap* and launch like this: `rlwrap -nN ./xmrtx.sh`. The arrow-up key will then recall previously issued commands._
* On **portrait-oriented** screens the menu will use less width. In any case it's recommended to also pass the terminal's width to the script: `COLUMNS=$COLUMNS rlwrap -nN ./xmrtx.sh`
* While interactive mode is active, wallet data is **saved regularily** according to a positive `SAVE_INTERVAL` value *and* after every successful request. To disable both, set `AUTOSAVE=no`.


## Tor

Most Linux flavors offer the package as `tor`.

As an example, the following commands typically suffice to enable Tor on Alpine Linux. Run them either as **root** or via *doas/sudo*:

    # apk add tor
    # rc-update add tor
    # rc-service tor start

Before launching the Monero RPC, **XMRPC** checks if Tor is available and an onion-node can be reached through it. If yes, it instructs Monero RPC to use both.

### Selection mechanism

* Tor is looked for at `DEFAULT_TOR_PROXY`, unless you specify `PROXY`, which precedes the default.
* In the effort to find a node, a list of nodes in `TOR_CANDIDATES` is iterated over until one is found to respond within the configured `NODE_TIMOUT`.
* If none of the known nodes respond, `TOR_FALLBACK` is used instead.


## Background Monero RPC

The command `./xmrpc.sh daemon` starts an RPC instance and leaves it running in the background.

Please refer to the documentation of your Linux flavor on how to configure its automatic execution upon boot.

As an example, again on Alpine Linux, executing the following as **root** or via *doas/sudo* should typically suffice:

    # # Replace <PATH_TO> with the correct path:
    # printf "#!/bin/sh\n<PATH_TO>/xmrpc.sh daemon" > /etc/local.d/xmrtx_monero_rpc.start
    # chmod +x /etc/local.d/xmrtx_monero_rpc.start
    # rc-update add local

In supervisors like *runit* that require services to stay in the foreground, `./xmrpc.sh daemon foreground` can be used instead.


## Roadmap

* search for available nodes in parallel
* notify about new transactions (`--tx-notify`)
* support i2p
