#!/bin/sh
#
# TOOLS curl bc jq openbsd-nc|nc(alpine:netcat-openbsd,debian:netcat-openbsd)
# TOOLS column(alpine:util-linux-misc,debian:bsdextrautils) sed|gsed(brew:gnu-sed)
# TOOLS monero-wallet-cli(monero) monero-wallet-rpc(monero) [rlwrap] [gpg]


determine_node_port () {
        port=0
        case "$NET" in
                stagenet) port=38081 ;;
                testnet)  port=28081 ;;
                mainnet)  port=18081 ;;
        esac

        echo "$port"
}


list_tor_candidates () {
        case "$NET" in
                stagenet)
                        # https://monero.fail/?chain=monero&network=stagenet&onion=on
                        echo 'plowsoffjexmxalw73tkjmf422gq6575fc7vicuu4javzn2ynnte6tyd.onion:38089'
                        echo 'plowsofe6cleftfmk2raiw5h2x66atrik3nja4bfd3zrfa2hdlgworad.onion:38089'
                        echo 'plowsof3t5hogddwabaeiyrno25efmzfxyro2vligremt7sxpsclfaid.onion:38089'
                        echo 'qz43zul2x56jexzoqgkx2trzwcfnr6l3hbtfcfx54g4r3eahy3bssjyd.onion:38081'
                        echo 'ykqlrp7lumcik3ubzz3nfsahkbplfgqshavmgbxb4fauexqzat6idjad.onion:38081'
                        ;;

                testnet)
                        # https://monero.fail/?chain=monero&network=testnet&onion=on
                        echo 'plowsofe6cleftfmk2raiw5h2x66atrik3nja4bfd3zrfa2hdlgworad.onion:28089'
                        echo 'plowsof3t5hogddwabaeiyrno25efmzfxyro2vligremt7sxpsclfaid.onion:28089'
                        echo 'qz43zul2x56jexzoqgkx2trzwcfnr6l3hbtfcfx54g4r3eahy3bssjyd.onion:28081'
                        echo 'lgyssws2oary5iclgvcwhwl6c65azwugvskf3iidelo2tzk4wpwblead.onion:28081'
                        ;;

                mainnet)
                        # https://monero.fail/?chain=monero&network=mainnet&onion=on
                        echo 'plowsoffjexmxalw73tkjmf422gq6575fc7vicuu4javzn2ynnte6tyd.onion:18089'
                        echo 'plowsofe6cleftfmk2raiw5h2x66atrik3nja4bfd3zrfa2hdlgworad.onion:18089'
                        echo 'plowsof3t5hogddwabaeiyrno25efmzfxyro2vligremt7sxpsclfaid.onion:18089'
                        echo 'libertytmtitynvmnto2k42liys5fenb3wabaozmmmksyrc7jvgmjiqd.onion:18089'
                        echo 'rucknium757bokwv3ss35ftgc3gzb7hgbvvglbg3hisp7tsj2fkd2nyd.onion:18081'
                        echo 'monerujods7mbghwe6cobdr6ujih6c22zu5rl7zshmizz2udf7v7fsad.onion:18081'
                        ;;
        esac
}


initialize () {
        readonly TOOLNAME="${TOOLNAME:-XMRPC}"
        readonly VERSION='0.2.0-beta'

        # shellcheck source=/_common/init.sh
        REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init.sh"
        # shellcheck source=/_common/functions.sh
        REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/functions.sh"

        NET="${NET:-stagenet}"
        readonly NETNAME="$NET"

        readonly NODE_TIMEOUT="${NODE_TIMEOUT:-5}"
        readonly NODE_TRUST="${NODE_TRUST:-untrusted}"
        readonly DEFAULT_TOR_PROXY='127.0.0.1:9050'
        readonly DEFAULT_I2P_PROXY='127.0.0.1:4447'
        PROXY="${PROXY:-}"

        DOMAIN1=xmr.id
        DOMAIN2=xmrid.com
        TOR_CANDIDATES=${TOR_CANDIDATES:-$(list_tor_candidates)}
        TOR_FALLBACK="${TOR_FALLBACK:-$(list_tor_candidates | head -n1)}"
        CLEARNET_CANDIDATES=${CLEARNET_CANDIDATES:-127.0.0.1:$(determine_node_port)}
        CLEARNET_FALLBACK="${CLEARNET_FALLBACK:-$NETNAME.community.rino.io:$(determine_node_port)}"

        # https://monero.fail/?chain=monero
        case "$NET" in
                stagenet)
                        DOMAIN1="${NETNAME}.${DOMAIN1}"
                        DOMAIN2="${NETNAME}.${DOMAIN2}"
                        RPC_PORT=${RPC_PORT:-38082}
                        ;;
                testnet)
                        RPC_PORT=${RPC_PORT:-28082}
                        ;;
                mainnet)
                        RPC_PORT=${RPC_PORT:-18082}
                        NET=
                        ;;
        esac
        readonly DOMAIN1 DOMAIN2 RPC_PORT

        readonly RPC_ENDPOINT="http://127.0.0.1:${RPC_PORT}/json_rpc"
        readonly RPC_BINARY="${RPC_BINARY:-monero-wallet-rpc}"
        readonly RPC_FILE="${RPC_FILE:-${XDG_CACHE_HOME}/monero/monero-wallet-rpc.$RPC_PORT.login}" 
        readonly RPC_LOGFILE="${TMPDIR:-/tmp}/monero-wallet-rpc.${NETNAME}.log"
        readonly RPC_LOGLEVEL="${RPC_LOGLEVEL:-0}"
        readonly MONERO_DIR="${MONERO_DIR:-${XDG_DATA_HOME}/monero}"
        readonly MONERO_FILE="${MONERO_FILE:-${MONERO_DIR}/${NETNAME}-${TOOLNAME}}"
        readonly MONERO_PASSWORD="${MONERO_PASSWORD:-xmr.zone}"
        readonly MONERO_PASSFILE="${MONERO_PASSFILE:-}"
        readonly TX_RINGSIZE=16
        readonly TX_PRIORITY=1 # 0-3 (default, unimportant, normal, elevated)
        readonly LOCK_BLOCKS=10
        readonly BLOCK_TIME=2
        readonly BASE_REQUEST='{"json_rpc":"2.0","id":"0"}'

        AUTOSAVE=${AUTOSAVE:-yes}
        SAVE_INTERVAL=${SAVE_INTERVAL:-300}

        echo "$TOOLNAME $VERSION" >> "$DEBUG"
}


obtain_rpc_credentials () {
        rpc_credentials=

        ocode=1
        if [ -f "${RPC_FILE}" ] ; then
                if [ -s "${RPC_FILE}" ] ; then
                        if rpc_credentials=$(cat "${RPC_FILE}") ; then
                                if echo "$rpc_credentials" | grep -q '.\+:.\+' ; then
                                        ocode=0
                                else
                                        ocode=5
                                        echo "ERROR: Content of RPC_FILE $RPC_FILE appears to be invalid." >> "$DEBUG"
                                fi
                        else
                                ocode=4
                                echo "ERROR: Unable to read from RPC_FILE $RPC_FILE." >> "$DEBUG"
                        fi
                else
                        ocode=3
                        echo "ERROR: RPC_FILE $RPC_FILE empty." >> "$DEBUG"
                fi
        else
                ocode=2
                echo "ERROR: Unable to find RPC_FILE $RPC_FILE." >> "$DEBUG"
        fi

        echo "$rpc_credentials"
        return $ocode
}


CURL () {
        request_string="${1:-}"

        ccode=0

        [ -n "$RPC_CREDENTIALS" ] ||
                RPC_CREDENTIALS=$(obtain_rpc_credentials)

        if [ -n "$RPC_CREDENTIALS" ] ; then
                curl --connect-timeout 1 --retry 2 \
                        -sSkX POST "${RPC_ENDPOINT}" \
                        -H 'Content-Type:application/json' \
                        --digest -u "${RPC_CREDENTIALS}" \
                        -d "$request_string" ; ccode=$?
        else
                ccode=1
                echo 'ERROR: No value for RPC_CREDENTIALS.' >> "$DEBUG"
        fi

        return $ccode
}


destroy_file () {
        target=$1 && shift

        type shred >/dev/null 2>&1 &&
                shred "$target"

        unlink "$target"
}


create_passfile () {
        content=$MONERO_PASSWORD
        [ -n "$MONERO_PASSFILE" ] &&
                content=$(get_contents_from_file "$MONERO_PASSFILE" | head -n1)

        passfile=$(umask 066 ; mktemp)
        echo "$content" > "$passfile"

        echo "$passfile"
}


start_rpc () {
        where=${1:-background}
        daemonize='--detach' ; [ "$where" = 'foreground' ] && daemonize=''
        proxy="--proxy $PROXY" ; [ -z "$PROXY" ] && proxy=''

        code=0
        if type "$RPC_BINARY" >/dev/null ; then
                [ -d "${RPC_FILE%/*}" ] || mkdir -p "${RPC_FILE%/*}"
                
                # shellcheck disable=SC2086
                if cd "${RPC_FILE%/*}" ; then
                        transient_password_file=$(create_passfile)
                        (sleep 3 && destroy_file "$transient_password_file")& d_pid=$!

                        $RPC_BINARY $daemonize $proxy \
                                --no-initial-sync \
                                --rpc-ssl disabled \
                                --rpc-bind-port="$RPC_PORT" \
                                --daemon-host "${NODE%%:*}" \
                                --daemon-port "${NODE##*:}" \
                                "--${NODE_TRUST}-daemon" \
                                --password-file "$transient_password_file" \
                                --wallet-file "$MONERO_FILE" \
                                --log-file="${RPC_LOGFILE}" --log-level "${RPC_LOGLEVEL}" \
                                "--$NET" 2>&1
                                #--wallet-dir  "$MONERO_DIR"  --password-file "$transient_password_file" \
                                #--offline
                        code=$?

                        wait $d_pid
                else
                        code=$?
                fi

        else
                code=127
                echo "Executable: $RPC_BINARY"
                echo 'RPC executable missing'
        fi

        return "$code"
}


daemon () {
        if echo "$1" | grep -w 'foreground' ; then
                enable_rpc "$@"
        else
                (enable_rpc "$@" >/dev/null) &
        fi
}


# Retrieves JSON from RPC
request () {
        method=$1 && shift
        params="${1:-}"

        params_string=.
        [ -n "$params" ] && params_string=".params={$params}"

        request_string=$(echo "${BASE_REQUEST}" | jq ".method=\"$method\"" | jq "$params_string")

        echo "RPC $method ..." >> "$DEBUG"

        code=0
        if response=$(CURL "${request_string}" 2>&1) ; then
                # When curl fails and then succeeds upon one of its --retry attempts
                # the code will be 0, but the previous error messages are still in
                # the variable and have to be removed before further processing.
                printf %s "$response" | grep '^curl' >&2
                printf %s "$response" | grep -vE '^curl'

                if error=$(printf %s "$response" | jq -r '.error' | grep -qvE '^null$') ; then
                        code=$(printf %s "$response" | jq -r '.error.code' | tr -d '-')
                fi
        else
                code=$?
                printf %s "$response"
        fi
        return "$code"
}


# Retrieves raw elements (non-JSON) with
# error-display through `handled_request`
get () {
        element=$1
        g_filter="${2:-.result.$element}"

        json_record=$(handled_request "get_$element" '') &&
                result=$(echo "$json_record" | jq -r '.result[]' | grep -v '^$') &&
                printf %s "$json_record" | jq -r "$g_filter" | grep -v '^$'
}


enable_wallet () {
        code=0
        if [ -f "$MONERO_FILE" ] ; then
                if [ ! -s "$MONERO_FILE" ] ; then
                        echo 'Monero file already exists but is empty'
                        code=201
                fi
        else
                create_wallet_via_cli ; code=$?
        fi

        if [ "$code" -eq 0 ] ; then
                echo 'Wallet present' >> "$DEBUG"
        else
                echo "Location: $MONERO_FILE"
                echo 'Monero file invalid'
        fi

        return "$code"
}



pgrep_rpc () {
        FULL_FILTER="^[^ ]*$RPC_BINARY .*--rpc-bind-port=$RPC_PORT"
        pgrep -f "$FULL_FILTER"
}


rpc_running () {
        additional_filter=${1:-}

        rr_code=1
        # Alternative that requires fuser: rpc_pid=$(fuser "$RPC_PORT/tcp" 2>/dev/null | xargs)
        if rpc_pid=$(pgrep_rpc) ; then
                if [ -n "$additional_filter" ]; then
                        ps -wwhp "$rpc_pid" -o args | grep -q "$additional_filter" &&
                                rr_code=0
                else
                        rr_code=0
                fi
        else
                rr_code=2
        fi

        # echo "$rpc_pid"
        return $rr_code
}


enable_rpc () {
        code=0
        if ! rpc_running ; then
                proxy_for_node && node && start_rpc "$@" ; code=$?
        fi

        if [ "$code" -eq 0 ] ; then
                echo 'RPC enabled' >> "$DEBUG"
        else
                echo 'RPC startup failed'
        fi
        return "$code"
}


locate_rpcfile () {
        code=0

        MAX_TRIES=10
        printf "RPC connection attempts" >> "$DEBUG"
        for s in $(seq 1 "$MAX_TRIES"); do
                printf " .. %s" "$s" >> "$DEBUG"

                if [ -e "${RPC_FILE}" ]; then
                        code=0
                        break
                else
                        code=211
                        [ "$s" -lt "$MAX_TRIES" ] && sleep 1
                fi
        done
        echo >> "$DEBUG"

        if [ "$code" -eq 0 ] ; then
                echo 'RPC file present' >> "$DEBUG"
        else
                echo "Credentials file $RPC_FILE not (yet) available"
                echo 'RPC starting or syncing'
        fi

        return "$code"
}


verify_rpcfile () {
        code=0

        if [ -f "${RPC_FILE}" ]; then
                [ ! -s "${RPC_FILE}" ] && sleep .1

                if [ -s "${RPC_FILE}" ] ; then
                        if grep -q '...:...' "${RPC_FILE}" ; then
                                code=0
                        else
                                code=212
                                echo "Credentials file $RPC_FILE has unexpected contents"
                        fi
                else
                        code=213
                        echo "Credentials file $RPC_FILE empty"
                fi
        else
                code=214
                echo "Credentials file type unexpected: $(file "$RPC_FILE")"
        fi

        if [ "$code" -eq 0 ] ; then
                echo 'RPC seems functional' >> "$DEBUG"
        else
                echo 'RPC file invalid'
        fi

        return "$code"
}


check_rpcfile () { locate_rpcfile && verify_rpcfile ; }


environment () { # Creates and starts everything necessary for RPC functionality
        # TODO consider calling `inspect` instead of `balance`
        enable_wallet && enable_rpc && locate_rpcfile && verify_rpcfile && balance
}


await_rpc_termination () {
        max_seconds=${1:-1}
        then=$(($(date +%s) + max_seconds))

        ar_code=1
        while [ "$(date +%s)" -lt "$then" ] || [ "$max_seconds" -lt 0 ]; do
                if rpc_running ; then
                        ar_code=$?
                        sleep .2
                else
                        ar_code=$?
                        break
                fi
        done

        return $ar_code
}


stop () {  # [await=1] # Gracefully stops the RPC, optionally blocking until gone
        await=${1:-1}

        if json_record=$(request "stop_wallet" '') ; then
                s_code=$?

                if echo "$await" | grep -q '^[0-9]\+$' ; then
                        await_rpc_termination "$await"
                else
                        await_rpc_termination -1
                fi
        else
                s_code=$?
        fi

        return "$s_code"
}


determine_nc_variant () {
        code=0

        if type nc >/dev/null ; then
                variant=
                nc_output=$(nc 2>&1)
                echo "$nc_output" | grep -qF '[-X proxy' && variant='openbsd'
                echo "$nc_output" | grep -qF 'Ncat' && variant='nmap'
                #echo "$nc_output" | grep -qF 'BusyBox' && variant='busybox'

                # BusyBox nc doesn't support proxies - only try to use if $PROXY unset
                # timeout "${NODE_TIMEOUT}" nc -z "${NODE%%:*}" "${NODE##*:}" 2>&1

                if [ -z "$variant" ] ; then
                        echo 'nc variant unknown'
                        code=249
                fi
        else
                code=$?
        fi

        echo "$variant"
        return "$code"
}


nmap_nc () {
        target=$1 && shift
        proxy=$1

        if [ -n "$proxy" ] ; then
                timeout "${NODE_TIMEOUT}" nc -z --proxy-type socks5 --proxy "${proxy}" "${target%%:*}" "${target##*:}" 2>&1
        else
                timeout "${NODE_TIMEOUT}" nc -z                                        "${target%%:*}" "${target##*:}" 2>&1
        fi
}


openbsd_nc () {
        target=$1 && shift
        proxy=$1

        if [ -n "$proxy" ] ; then
                timeout "${NODE_TIMEOUT}" nc -z -X 5 -x "${proxy}" "${target%%:*}" "${target##*:}" 2>&1
        else
                timeout "${NODE_TIMEOUT}" nc -z                    "${target%%:*}" "${target##*:}" 2>&1
        fi
}


determine_node_type () {
        node_type=$(echo "$NODE" | sed 's#^[[:alnum:]]\+.\([^:]\+\):[0-9]\+#\1#g')
        [ -n "$node_type" ] && echo "$node_type" || return 1
}


check_proxy () {        # <host:port> # Checks proxy availability [non-RPC]
        server=$1 && shift
        
        code=0
        if variant=$(determine_nc_variant) ; then
                case "$variant" in
                        nmap)    nmap_nc    "$server" ; code=$? ;;
                        openbsd) openbsd_nc "$server" ; code=$? ;;
                esac
        else
                code=$?
        fi

        return "$code"
}


report_error_on_proxy () {
        code=$1 && shift

        printf "Proxy: %b\n" "${PROXY}"
        [ "$code" = 124 ] && echo "Timeout: ${NODE_TIMEOUT}s reached"
        echo "Please call this with your Tor-proxy details prefixed. Example: PROXY=<IP:PORT> ./$(basename "$0")"
        echo 'Proxy connection failed'
}


determine_proxy () {
        dp_code=0

        # Never override a user-provided proxy
        if [ -z "$PROXY" ] ; then
                if [ -n "$NODE" ] ; then
                        case $(determine_node_type) in
                                onion) check_proxy "$DEFAULT_TOR_PROXY" && PROXY=$DEFAULT_TOR_PROXY || dp_code=$? ;;
                                i2p)   check_proxy "$DEFAULT_I2P_PROXY" && PROXY=$DEFAULT_I2P_PROXY || dp_code=$? ;;
                                *)     PROXY= ;;
                        esac
                else
                        for p in "$DEFAULT_I2P_PROXY" "$DEFAULT_TOR_PROXY" ; do
                                check_proxy "$p" && PROXY=$p
                        done
                fi
        fi

        readonly PROXY

        return "$dp_code"
}


check_node () {        # <host:port> # Checks node availability [non-RPC]
        host=$1 && shift

        cn_code=0

        if [ -n "$host" ] ; then
                if variant=$(determine_nc_variant) ; then
                        case "$variant" in
                                nmap)    nmap_nc    "$host" "$PROXY" ; cn_code=$? ;;
                                openbsd) openbsd_nc "$host" "$PROXY" ; cn_code=$? ;;
                        esac
                else
                        cn_code=$?
                fi
        else
                cn_code=19
        fi

        return "$cn_code"
}

select_connectable_node_from () {
        candidates=$*

        sc_code=1
        for c in $candidates ; do
                if check_node "$c" ; then
                        node_name=${c%%:*}
                        node_port=${c##*:}
                        sc_code=0
                        break
                fi
        done

        echo "$node_name:$node_port"
        return $sc_code
}

proxy_for_node () {
        nap_code=0

        # Never override a user-provided node
        if [ -z "$NODE" ] ; then
                if [ -n "$PROXY" ] ; then
                        check_proxy "$PROXY" ; nap_code=$?
                else
                        determine_proxy
                fi

                if [ "$nap_code" -eq 0 ] ; then
                        if [ -n "$PROXY" ] ; then
                                NODE=$(select_connectable_node_from "$TOR_CANDIDATES")
                                [ "${#NODE}" -ge 3 ] || NODE="$TOR_FALLBACK"
                        else
                                NODE=$(select_connectable_node_from "$CLEARNET_CANDIDATES")
                                [ "${#NODE}" -ge 3 ] || NODE="$CLEARNET_FALLBACK"
                        fi
                else
                        nap_code=1
                        NODE=
                fi
        fi
        readonly NODE

        case "$nap_code" in
              0) echo "$NODE" ;;
              *) report_error_on_proxy "$nap_code" ;;
        esac

        return "$nap_code"
}


node () {
        code=0

        if check_node "$NODE" ; then
                echo "${NODE}"
                echo 'Node available' >> "$DEBUG"
        else
                code=$?
                printf "Node: %b\n" "${NODE}"
                [ -n "$PROXY" ] && printf " (via proxy %b)\n" "${PROXY}"
                [ "$code" = 124 ] && echo "Timeout: ${NODE_TIMEOUT}s reached"
                echo 'Node connection failed'
        fi

        return "$code"
}


determine_seed_language () {
        seedlang='English'
        case "$LANG" in
                de*)  seedlang='German' ;;
                en*)  seedlang='English' ;;
                es*)  seedlang='Spanish' ;;
                fr*)  seedlang='French' ;;
                it*)  seedlang='Italian' ;;
                nl*)  seedlang='Dutch' ;;
                pt*)  seedlang='Portuguese' ;;
                ru*)  seedlang='Russian' ;;
                jp*)  seedlang='Japanese' ;;
                cn*)  seedlang='Chinese (simplified)' ;;
                eo*)  seedlang='Esperant' ;;
                jbo*) seedlang='Lojban' ;;
        esac

        echo "$seedlang"
}


create_wallet_via_cli () {
        seedlang=$(determine_seed_language)

        [ -d "${MONERO_FILE%/*}" ] || mkdir -p "${MONERO_FILE%/*}"

        # As of 2024-05-13, on stagenet, when using the current +%Y-%m-%d
        # as restore-date, "refresh-from-block-height" would get set to
        # roughly four days (~2660 blocks) into the future, making it
        # impossible to ever have the RPC scan and find early funds received.
        #
        # According to selsta this will be fixed in the next release following 0.18.3.3.
        #
        # Workaround:
        TEN_DAYS=864000
        now=$(date +%s)
        restore_date="$now" ; [ "$NET" = 'stagenet' ] && restore_date=$((now - TEN_DAYS))

        transient_password_file=$(create_passfile)
        (sleep .3 && destroy_file "$transient_password_file")& d_pid=$!

        monero-wallet-cli "--$NET" --offline --restore-date "$(date -d"@${restore_date}" +%Y-%m-%d)" \
                --generate-new-wallet "$MONERO_FILE" --password-file "$transient_password_file" \
                --mnemonic-language "${seedlang}" --use-english-language-names \
                --log-file /dev/null --log-level 0 --command 'welcome' 

        wait $d_pid
}


# https://docs.getmonero.org/rpc-library/wallet-rpc/#get_height
height () { get 'height' || return $? ; }   # Shows current block height

# https://docs.getmonero.org/rpc-library/wallet-rpc/#get_address
address ()   { get 'address'                       || return $? ; } # Shows our primary Monero account number
addresses () { get 'address' '.result.addresses[]' || return $? ; } # Shows all our Monero account numbers

# https://docs.getmonero.org/rpc-library/wallet-rpc/#get_balance
balance () {        # Shows available balance
        code=0
        if json_record=$(get 'balance' '.result') ; then
                unlocked=$(echo "$json_record" | jq -r '.unlocked_balance')
                balance=$(echo "$json_record" | jq -r '.balance')

                REMOVE_IRRELEVANT_ZEROS='/\./ s/\.\{0,1\}0\{1,\}$//'
                available=$(printf %b "scale=12; ${unlocked} / 1000^4 + .0\n" | bc | sed "$REMOVE_IRRELEVANT_ZEROS")
                if [ ! "${balance}" = "${unlocked}" ] ; then
                        available="${available}/$(printf %b "scale=0; ${balance} / 1000^4\n" | bc)"
                fi

                if [ "${unlocked}" -gt 0 ] ; then
                        printf "%s\n" "${available}"
                else
                        bal=$(printf %b "scale=12; ${balance} / 1000^4\n" | bc)
                        if [ "${balance}" -gt 0 ] ; then
                                echo 'None of your Monero are spendable at this moment'
                                blocks=$(echo "$json_record" | jq -r '.blocks_to_unlock')
                                [ "$blocks" -eq 0 ] && blocks=$((LOCK_BLOCKS * BLOCK_TIME))
                                seconds=$(echo "$json_record" | jq -r '.time_to_unlock')
                                minutes=$((seconds/60)) 
                                [ "$minutes" = '0' ] && minutes=$((blocks * BLOCK_TIME))
                                echo "Please wait some $minutes minutes for their release"
                                echo "Balance: $bal"
                                echo "Blocks until unlock: $blocks"
                                code=231
                        else
                                own=$(address)
                                echo 'Your balance is ZERO'
                                [ "$NET" = 'stagenet' ] &&
                                        from=' from https://community.rino.io/faucet/stagenet/'
                                echo "Send some Monero${from} to your account at ${own}"

                                code=230
                        fi
                        #echo "Own: $own"
                        echo 'Balance locked'
                fi
        else
                code=$?
                echo "$json_record"
                echo 'Balance request failed'
        fi

        return "$code"
}


process_json_error () {
        json_record=$1 && shift

        code=0
        if error=$(printf %s "$json_record" | jq -r '.error' | grep -qvE '^null$') ; then
                err_message=$(printf %s "$json_record" | jq -r '.error.message')
                err_code=$(echo "$json_record" | jq -r '.error.code' | tr -d '[:punct:]')
                if [ "$err_code" != 'null' ] ; then
                        [ "$err_code" -gt 0 ] && code="$err_code"
                        echo "$err_message"
                else
                        echo "$json_record"
                        echo "$error" >> "$DEBUG"
                fi

        else
                code=$?
        fi

        return "$code"
}


# Calls `request` with added error-output
handled_request () {
        request_name=$1 && shift
        params=$1 && shift

        code=0
        if json_record=$(request "$request_name" "$params") ; then
                echo "$json_record"
        else
                code=$?
                echo "$json_record"
                echo "$request_name request failed"
        fi
        return "$code"
}


# https://docs.getmonero.org/rpc-library/wallet-rpc/#label_address
# <sub_idx> <label> [acc_idx] # Labels the address at the given index
label_address () {
        minor=$1 && shift
        label=$1 && shift
        major=${1:-0}

        handled_request 'label_address' \
                "index:{major:${major},minor:${minor}},label:\"${label}\""
}


# https://docs.getmonero.org/rpc-library/wallet-rpc/#create_address
create_address () {
        label=$1 && shift # e.g. "XMRID ${XMRID}"

        handled_request 'create_address' \
                "account_index:0,label:\"${label}\""
}

# https://docs.getmonero.org/rpc-library/wallet-rpc/#incoming_transfers
incoming () { # [index] # Shows transfers, optionally filtered by subaddress index
        subaddr_index=${1:-}

        handled_request 'incoming_transfers' \
                "transfer_type:\"all\",subaddr_indices:[${subaddr_index}]"
}

# https://docs.getmonero.org/rpc-library/wallet-rpc/#get_transfers
outgoing () { # [min_height] # Shows spends, optionally filtered by oldest block to consider
        min_height=${1:-1}

        threshold_height=$((min_height - 1))

        handled_request 'get_transfers' \
                "out:true,filter_by_height:true,min_height:${threshold_height}"
}

# https://docs.getmonero.org/rpc-library/wallet-rpc/#transfer
transfer () {   # <dest> <amount> # Transfers amount to destination (INSTANT!)
        destination=$1 && shift
        amount=$1 && shift

        code=0

        # 2023-05: RPC v0.18.1.2 would crash when attempting to transfer to an invalid $destination
        if validate "$destination" ; then

                piconero=$(echo "scale=12; $amount * 1000^4" | bc)
        
                transfer_request="destinations:[{amount:${piconero},address:\"${destination}\"}],ring_size:${TX_RINGSIZE},priority:${TX_PRIORITY}"
                #echo "$transfer_request" | jq
        
                if json_record=$(request 'transfer' "$transfer_request") ; then

                        if result=$(echo "$json_record" | jq -r '.result' | grep -v '^$') ; then
        
                                if piconero=$(echo "${result}" | jq -r '.amount' | grep -v '^$' ) ; then
                                        fee=$(echo "${result}" | jq -r '.fee')
                                        tx_hash=$(echo "${result}" | jq -r '.tx_hash')
                                        total=$(echo "scale=12; ($piconero + $fee) / 1000^4" | bc)
        
                                        printf "%s %s\n" "$tx_hash" "$total"
                                else
                                        code=$?
                                        printf %s "$json_record"
                                fi
                        else
                                code=$?
                                process_json_error

                        fi
                else
                        code=$?
                        process_json_error "$json_record"
                        echo "Unable to send $amount Monero"
                        echo 'Be sure to have enough to account for transaction fees'
                        echo 'Transfer FAILED'
                fi
        else
                code=$?
        fi

        return "$code"
}


# https://docs.getmonero.org/rpc-library/wallet-rpc/#validate_address
validate_address () {   # <str> # Verifies a string as Monero destination
        destination=$1 && shift
        va_filter="${2:-.result.nettype}"

        code=0
        if json_record=$(request 'validate_address' "address:\"${destination}\"") ; then
                echo "$json_record" >> "$DEBUG"

                if nettype=$(echo "$json_record" | jq -r "$va_filter" | grep -v '^$') ; then
                        echo "$destination is valid on $nettype" >> "$DEBUG"
                else
                        code=$?
                        echo "Invalid destination: $destination"
                fi
        else
                code=$?
                echo 'Request failed'
        fi

        return "$code"
}


validate () {
        str=$1 && shift

        STANDARD_LEN=95
        INTEGRATED_LEN=106

        [ "${#str}" -eq "$STANDARD_LEN" ] ||
                [ "${#str}" -eq "$INTEGRATED_LEN" ] && validate_address "$str"
}


# https://docs.getmonero.org/rpc-library/wallet-rpc/#store
store () { request "store" '' >/dev/null ; }


inspect () { # [filter] # Shows RPC process or script variables [non-RPC]
        filter=$1

        sh_code=0
        if rpc_pid=$(pgrep_rpc) ; then
                if [ -n "$filter" ] ; then
                        set | grep -i "^$filter" | grep '='
                else
                        process=$(ps -wwhp "$rpc_pid" -o args | FMT)
                        echo "[${rpc_pid}@$(hostname)] $process"
                fi
        else
                sh_code=$?
                echo "ERROR: No process matches $RPC_BINARY at port $RPC_PORT" >> "$DEBUG"
        fi

        return $sh_code
}


help () { common_help ; }      # Lists documented functions [non-RPC]


quit () { common_quit ; }      # Exits the program [non-RPC]


execute () {
	action=${1:-help}
        [ -n "$1" ] && shift

        case "$action" in
                a|address|get_address)    address ;;
                b|balance|get_balance)    balance ;;
                c|create_address)         create_address "$@" ;;
                d|addresses)              addresses ;;
                e|environment)            environment ;;
                g|get)                    get "$@" ;;
                h|height|get_height)      height ;;
                i|incoming)               incoming "$@" ;;
                l|label|label_address)    label_address "$@" ;;
                n|node|check_node)        check_node "$@" ;;
                o|outgoing)               outgoing "$@" ;;
                p|proxy|check_proxy)      check_proxy "$@" ;;
                q|quit|exit)              quit ;;
                r|request)                request "$@" ;;
                s|store)                  store ;;
                t|transfer)               transfer "$@" ;;
                v|validate)               validate "$@" ;;

                inspect)                  inspect "$@" ;;
                daemon)                   daemon "$@" ;;
                wallet|enable_wallet)     enable_wallet ;;
                rpc|enable_rpc)           enable_rpc "$@" ;;
                stop|stop_rpc)            stop "$@" ;;
                rpc_running)              rpc_running ;;
                rpcfile|check_rpcfile)    check_rpcfile ;;

                # Exposed for use by test suite
                proxy_for_node)           proxy_for_node ;;
                determine_proxy)          determine_proxy ;;

                help)                     help ;;
                *)                        help && printf 'Invalid command "%s"\n' "$action" ;;
        esac
}


save_in_background () {
        b_code=1

        if [ -n "$autosave_pid" ] && ps -p "$autosave_pid" >/dev/null ; then
                echo "WARNING: The RPC is currently saving other data. Skipping write to wallet file." >> "$DEBUG"
        else
                (store)& autosave_pid=$!
                b_code=0
        fi

        return "$b_code"
}


interval_save () {
        i_code=0

        if [ "$(jq -n "${SAVE_INTERVAL:-0} > 0")" = 'true' ] ; then
                while true ; do
                        sleep "$SAVE_INTERVAL"
                        save_in_background
                done
        else
                echo "ERROR: SAVE_INTERVAL must be a number greater than zero (current value: $SAVE_INTERVAL)" >> "$DEBUG"
                i_code=1
        fi

        return $i_code
}


status_flags () {
        rpc_active='-' ; node_type='-'

        if rpc_running ; then
                rpc_active="$(printf '%.1s' "$NETNAME" | tr '[:lower:]' '[:upper:]')"
                if rpc_running ' --daemon-host [^ ]\+\.onion ' ; then
                        node_type='T' # Tor
                else
                        if rpc_running ' --daemon-host [^ ]\+\.i2p ' ; then
                                node_type='I' # i2p
                        else
                                node_type='C' # clearnet
                        fi
                fi
        fi

        echo "${rpc_active}${node_type}"
}


initialize

if [ -n "$1" ]; then
        execute "$@"
else
        [ "$AUTOSAVE" = 'yes' ] && (interval_save)&

        help
        while true ; do
                printf '[%s] Execute: ' "$(status_flags)" && read -r action && clear
                # shellcheck disable=SC2086
                (execute $action) ; code=$?

                [ "$code" -eq   0 ] &&
                        [ "$AUTOSAVE" = 'yes' ] && [ "$action" != 'store' ] &&
                        save_in_background

                [ "$code" -eq 255 ] && exit
                [ "$code" -gt   0 ] && echo && echo "Error $code"
        done
fi
