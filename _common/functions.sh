#!/bin/sh
#
# Functions required by multiple tools of the sxmo-onfire suite.
#
# TOOLS  awk sed|gsed(brew:gnu-sed) fmt tr column [gpg]



FMT () {
	right_indent=${1:-4}
	fmt -"$((${COLUMNS:-55} - right_indent))" 2>/dev/null || cat
}


determine_orientation () {
        orientation='landscape'

        do_code=0

        if type xrandr >/dev/null 2>&1 && xrandr >/dev/null 2>&1 ; then
                if resolution=$(xrandr | grep -F '*' | awk '{ print $1 }' |  sed 's|x|\/|g') ; then
                        [ $(($(printf '%s' "$resolution"))) -eq 0 ] &&
                                orientation='portrait'
                else
                        do_code=$?
                fi
        else
                do_code=$?
        fi

        echo "$orientation"
        return $do_code
}


enough_width_available () {
        MIN_WIDTH=55

        limited=0
        if [ -n "$COLUMNS" ] ; then
                [ "$COLUMNS" -lt "$MIN_WIDTH" ] &&
                        limited=1
        else
                [ "$(determine_orientation)" = 'portrait' ] &&
                        limited=1
        fi

        return "$limited"
}


# to be used with stdin
align_descriptions () {
	entries=$(cat)

        if enough_width_available ; then
		entries=$(echo "$entries" | column -ts '#' | sed 's|^|    |g')
	else
		entries=$(echo "$entries" | grep -vE '^help|^quit' | sed 's|#|\n|g' | FMT | sed 's|^| |g' )
	fi

	echo "$entries"
}


common_help () {       # Lists documented functions [non-RPC]
        echo "${TOOLNAME-CONSOLE} ${VERSION:-beta} by fullmetalScience"
        #if [ "$NET" != 'stagenet' ] && [ "$NET" != 'testnet' ] ; then
                echo
                echo '    WARNING: All commands execute without asking confirmation!' | FMT 8
        #fi
        echo
        #MATCH_FUNCTIONS='s|^\([^ ]\+\).*()[^#]*\(#.*\)|\1\2|p'
        #REMOVE_FIRST_HASHSIGN='s|#\(.*\)#\(.*\)|\1#\2|g'
        MATCH_FUNCTIONS='/^[^ ^#]+ +\(\) .*#.+/'
        FUNCTION_NAME='\([^\(]\+\)'
        REMOVE_FIRST_HASHSIGN='s|\(.*\)#\(.*\)#\(.*\)|\1\2#\3|g'

        awk "$MATCH_FUNCTIONS" "$0" | \
                sed "s|$FUNCTION_NAME [^\#]\+\# \(.*\)|\1\# \2|g  ;  $REMOVE_FIRST_HASHSIGN" |
                align_descriptions
        echo
}


common_quit () { exit 255 ; }      # Exits the program [non-RPC]



get_contents_from_file () {
        g_file=$1 && shift

        g_code=1
        g_contents=
        if [ -s "$g_file" ] ; then
                case "$g_file" in
                        *.gpg)
                                if type gpg >/dev/null 2>&1 ; then
                                        g_contents=$(gpg --quiet --decrypt "$g_file") ; g_code=$?
                                else
                                        g_code=$?
                                fi
                                ;;
                        *)
                                g_contents=$(cat "$g_file") ; g_code=$?
                                ;;
                esac
        else
                g_code=2
        fi

        echo "$g_contents"
        return $g_code
}
