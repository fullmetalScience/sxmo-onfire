#!/bin/sh
#
# This script is meant to be sourced only.
#
# It determines which tools to use and sets reasonable defaults for variables
# such as $DATA and $DEBUG.

# shellcheck source=_common/assert_environment.sh
REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/assert_environment.sh"
DIRPATH=${REALPATH%/*}

XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
DATA="${XDG_DATA_HOME}/sxmo-onfire/${DIRPATH##*/}"
XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
CACHE="${XDG_CACHE_HOME}/sxmo-onfire/${DIRPATH##*/}"
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"


ONFIRE_COLOR1=069

DEBUG=/dev/null
#DEBUG=/dev/stderr
#DEBUG=/dev/stdout


{
        echo "DIRPATH: $DIRPATH"
        echo "Colors: $ONFIRE_COLOR1"
        echo "CACHE: $CACHE"
        echo "DATA: $DATA"
} >> "$DEBUG"
