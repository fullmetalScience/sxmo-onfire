#!/bin/sh
#
# This script is meant to be sourced after "init.sh", by those tools that
# requiring dmenu/bemenu. It determines which exact executables to use.


# TODO update for new Sxmo environment (has sxmo_hook_icons.sh been replaced?)
# shellcheck source=configs/default_hooks/sxmo_hook_icons.sh disable=SC1091
type sxmo_hook_icons.sh >/dev/null 2>&1 &&
        . "$(which sxmo_hook_icons.sh)"

# shellcheck source=scripts/core/sxmo_common.sh disable=SC1091
type sxmo_common.sh >/dev/null 2>&1 &&
        . "$(which sxmo_common.sh)"

MENU=dmenu
if type sxmo_dmenu.sh >/dev/null 2>&1 ; then
        MENU=sxmo_dmenu.sh
else
        if type bemenu >/dev/null 2>&1 ; then
                MENU=bemenu
        fi
fi
MENU="$MENU ${SXMO_DMENU_OPTS:-} -l ${SXMO_DMENU_LANDSCAPE_LINES:-15}"
MENU_WITH_KB="$MENU -l ${SXMO_DMENU_LANDSCAPE_LINES:-10}"
if type sxmo_dmenu_with_kb.sh >/dev/null 2>&1 ; then
        MENU_WITH_KB="sxmo_dmenu_with_kb.sh"
fi


{
        echo "Menus: $MENU"
        echo "Menus with keyboard: $MENU_WITH_KB"
        echo "Colors: $ONFIRE_COLOR1"
} >> "$DEBUG"
