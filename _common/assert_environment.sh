#!/bin/sh
#
# Verifies required tools and files as provided in script file comments
#
# e.g. # TOOLS bemenu|dmenu wl-copy|xclip msmtp
# e.g. # FILES "$HOME/.msmtprc"
#
#
# Features
#
# * Alternatives may separated by the pipe symbol: xsel|xclip|wl-paste
# * Multiple lines are respected as long as they begin with a supported keyword ("TOOLS", "FILES")
# * Optional tools can be mentioned like "[barcode_gen]" and will be ignored
#
# TODO Prefix function names employed here in order to avoid clashing with those defined in any scripts that source this one
# TODO Suggested packages may be provided in parenthesis: msmtp(msmtp)
# TODO Warn when optional tools are missing
# TODO Ignore contents after hash-sign: # ignored_tool
# TODO Suggested packages by Linux distribution may be provided with colon: msmtp(alpine:msmtp debian:msmtp)
#      On Alpine, /etc/apk/world contains a list of installed packages.
#
# This script works either by simply being sourced or by being called with a
# script name to check as first and only argument.


DEBUG=${DEBUG:-/dev/null}
#DEBUG=/dev/stderr

# Support GNU-sed on macOS as installed by brew
SED='sed' ; type gsed >/dev/null 2>&1 && SED=gsed

get_required () {
        TYPE=$1 && shift
        SCRIPT=$1 && shift

        MATCH_OPTIONALS='^\[.*\]$'
        # TODO check if gsed is present
        $SED -n "s|^# *$TYPE \+\(.*\)|\1|p" "$SCRIPT" | $SED 's| |\n|g' |
                grep -v "$MATCH_OPTIONALS" || return 0
}

check_single () {
        TYPE=$1 && shift
        RESOURCE=$(eval echo "$1") && shift

        code=0
        case $TYPE in
                executable) type "$RESOURCE" >/dev/null ; code=$? ;;
                file)       test -f "$RESOURCE"         ; code=$? ;;
        esac
        return "$code"
}

get_missing () {
        TYPE=$1 && shift
        REQUIRED=$1 && shift

        errors=0
        missing=
        for r in $REQUIRED ; do
                code=9
                ALTERNATIVES=$(echo "$r" | $SED 's#|#\n#g')
                for a in $ALTERNATIVES ; do
                        check_single "$TYPE" "${a%%\(*}" ; code=$?
                        [ "$code" -eq 0 ] && break
                done


                if [ "$code" -eq 0 ] ; then
                        echo "Required $TYPE $r is present." >> "$DEBUG"
                else
                        echo "Required $TYPE $r missing." >&2
                        missing="$missing$r "
                        errors=$((errors + 1))
                fi
        done

        echo "$missing"
        return "$errors"
}

TERMINAL () {
        cmd_sequence=$1 && shift
        # TODO check for available terminals and use one of those
        foot sh -c "$cmd_sequence"
}

output_errors () {
        # Do not pass any strings containing single-quotes (')
        messages=$1 && shift

        if [ -t 2 ] ; then
                # shellcheck disable=SC2059  # $messages contains control sequences that shall be interpreted
                printf "$messages" >&2
        else
                TERMINAL "printf '$messages' >&2 && printf '\n\nPress ENTER to exit.' && read -r key"
        fi
}

assert_environment_main () {
        REQUIRED_FILES=$(get_required FILES "$SCRIPT")
        REQUIRED_TOOLS=$(get_required TOOLS "$SCRIPT")

        missing_files=$(get_missing file "$REQUIRED_FILES") ; fail=$?
        missing_tools=$(get_missing executable "$REQUIRED_TOOLS") ; fail=$((fail + $?))
        
        messages=
        if [ -n "$missing_files" ] ; then
                messages="\nBe sure to put these files in place:\n\t$missing_files\n"
        fi
        if [ -n "$missing_tools" ] ; then
                messages="${messages}\nBe sure to install these tools:\n\t$missing_tools\n"
                if type apk >/dev/null 2>&1 && type doas >/dev/null 2>&1 ; then
                        first_tool=${missing_tools%% *}
                        messages="${messages}(try \"doas apk add ${first_tool%%|*}\")"
                fi
        fi

        if [ -n "$messages" ] ; then
                output_errors "$messages"
        else
                echo "No error messages." >> "$DEBUG"
        fi
}


# for when being sourced
SCRIPT=${0}
ARGUMENT=${1:-}

# for when called with a script to check as argument
# TODO If the script that sources this itself receives arguments
#      $@ will be passed here. Testing for -f is just a workaround. 
#      Instead, a manner should be found to source this with empty $@.
[ -n "${ARGUMENT}" ] && [ -f "${ARGUMENT}" ] && SCRIPT=${ARGUMENT}

assert_environment_main

unset DEBUG

[ "$fail" -gt 0 ] && exit "$fail" || return $fail
