#!/bin/sh
#
# TOOLS  curl bc jq sed|gsed(brew:gnu-sed) tr column [rlwrap]


initialize () {
        readonly TOOLNAME='OGRE'
        readonly VERSION='0.1.0'

        # shellcheck source=/_common/init.sh
        REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init.sh"
        # shellcheck source=/_common/init_gui.sh
        REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/init_gui.sh"
        # shellcheck source=/_common/functions.sh
        REALPATH=$(realpath "${0}") && . "${REALPATH%/*}/../_common/functions.sh"
        

        readonly PASSFILE="${PASSFILE:-}"
        # FEE_FACTOR is used in max-quantity trades to get as close as possible
        # to actually using all funds available. Due to the lack of other methods,
        # it was obtained through trial-and-error.
        readonly FEE_FACTOR=.998004005

        i_code=0
        API_KEY="${API_KEY:-}"
        API_SECRET="${API_SECRET:-}"
        if [ -n "$PASSFILE" ] ; then
                API_CREDENTIALS=$(get_contents_from_file "$PASSFILE" | grep ^API)

                API_KEY=$(echo "$API_CREDENTIALS" | grep '^API_KEY' | sed 's|=| |g' | awk '{ print $2 }')
                API_SECRET=$(echo "$API_CREDENTIALS" | grep '^API_SECRET' | sed 's|=| |g' | awk '{ print $2 }')
        fi
        readonly API_KEY API_SECRET

        if [ -z "$API_KEY" ] || [ -z "$API_SECRET" ] ; then
                echo 'ERROR: API credentials incomplete!' >&2
                echo '       Please provide API_KEY and API_SECRET or a PASSFILE containing both. Examples:' >&2
                echo >&2
                echo "       API_KEY=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa API_SECRET=bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb ./$0" >&2
                echo "       PASSFILE=~/.password-store/tradeogre.gpg ./$0" >&2
                echo "       PASSFILE=~/tradeogre-credentials.txt ./$0" >&2
                i_code=1
        fi

        return $i_code
}


CURL () {
        method="$1" && shift
        endpoint="$1" && shift
        request_string="$1"

        # TradeOgre blocks Tor exit nodes :/
        #proxy= ; [ -n "$PROXY" ] && proxy="--proxy socks5h://$PROXY"

        echo "$method https://tradeogre.com/api/v1${endpoint}" >> "$DEBUG"
        curl --connect-timeout 4 --retry 2 \
                -sSX "$method" "https://tradeogre.com/api/v1${endpoint}" \
                -u "$API_KEY:$API_SECRET" -d "$request_string"
}


markets () {
        CURL 'GET' "/markets" | jq -r '.[] | to_entries[] | .key'
}


ticker () {  # [market=XMR-BTC] # Shows basic data about a market
        CURL 'GET' "/ticker/${1:-XMR-BTC}" |
                jq -r 'to_entries[]  |  select(.key == "price" or .key == "low" or .key == "high")  |  [.key, .value]  |  @csv' |
                column -ts, | tr -d \"
}


funds () {  # Shows all funds held on platform
        CURL 'GET' '/account/balances' |
                jq -r '.balances | to_entries[] |  select(.value != "0.00000000" ) | [.key, .value] | @tsv'
}


available () {  # <currency> # Shows available balance for currency
        currency=${1} && shift
        CURL 'POST' '/account/balance' "currency=$currency" | jq -r '.available'
}


balance () {  # [currencies=XMR BTC] # Shows balances for requested currencies
        currencies=${*:-XMR BTC}
        for c in $(echo "$currencies" | tr '[:lower:]' '[:upper:]') ; do
                CURL 'POST' '/account/balance' "currency=$c" |
                        jq -r '.currency="'"$c"'"| to_entries[] |  select(.key == "balance" or .key == "available" or .key == "currency" ) |  [.key, .value] | @csv'
        done | column -ts, | tr -d \"
}


orders () {  # [market] # Shows active orders
        market=$1
        [ -n "$market" ] && query="market=$1"
        json_response=$(CURL 'POST' "/account/orders" "$query") 

        echo "$json_response" |
                jq -r '.[] | to_entries[] |  select(.key == "uuid" or .key == "type" or .key == "price" or .key == "quantity" or .key == "market" ) | [.key, .value] | @csv' |
                column -ts, | tr -d \"
}


cancel () {  # <uuid> # Cancels an order
        uuid=$1 && shift

        json_response=$(CURL 'POST' '/order/cancel' "uuid=$uuid")
        if echo "$json_response" | grep -qxF '{"success":true}' ; then
                rm "$CACHE/$uuid.json"
        fi

        echo "$json_response" |
                jq -r 'to_entries[] |  select(.key == "success" or .key == "error") | [.key, .value] | @tsv'
}


cache () {
        operation=$1 && shift
        market=$1 && shift
        quantity=$1 && shift
        price=$1 && shift
        json_response=$1 && shift

        case "$operation" in
                buy|sell)
                        if mkdir -p "$CACHE" ; then
                                uuid=$(echo "$json_response" | jq -r '.uuid')
                                data="{\"type\":\"$operation\", \"price\":\"$price\",
                                       \"quantity\":\"$quantity\", \"market\":\"$market\"}"
                                echo "$json_response" | jq ".+= $data" > "$CACHE/$uuid.json"
                        fi
                        ;;
        esac
}


trade () {
        operation=$1 && shift
        market=$1 && shift
        quantity=$1 && shift
        price=$1 && shift

        if json_response=$(CURL 'POST' "/order/$operation" "quantity=$quantity&price=$price&market=$market") ; then
                echo "$json_response" | jq -r 
                cache "$operation" "$market" "$quantity" "$price" "$json_response"
        fi
}


BUY () {  # <price> [quantity=MAX] [market=XMR-BTC] # Buys base (left) <= spends quote (right)
        price=$1 && shift
        quantity=${1:-MAX}
        market=${2:-XMR-BTC}

        if [ "$quantity" = 'MAX' ] ; then
                if quote_quantity=$(available "${market##*-}") ; then
                        # TODO Check if "bc"-dependency can be dropped in favor of "jq -n".
                        quantity=$(echo "scale=12; ($quote_quantity / $price) * $FEE_FACTOR" | bc)
                        echo "Ordering $quantity ${market%%-*} ..."
                fi
        fi

        trade buy "$market" "$quantity" "$price"
}


SELL () {  # <price> [quantity=MAX] [market=XMR-BTC] # Sells base (left) => obtains quote (right)
        price=$1 && shift
        quantity=${1:-MAX}
        market=${2:-XMR-BTC}

        [ "$quantity" = 'MAX' ] &&
                quantity=$(available "${market%%-*}")

        echo "Offering $quantity ${market%%-*} ..."

        trade sell "$market" "$quantity" "$price"
}


help () { common_help ; }      # Lists documented functions


quit () { common_quit ; }      # Exits the program


execute () {
	action=${1:-help}
        [ -n "$1" ] && shift

        case "$action" in
                f|fu|fun|fund|funds)                                    echo Funds        && echo && funds          | sed 's|^|    |g' ;;
                a|av|ava|avai|avail|availa|availab|availabl|available)  echo Available    && echo && available "$@" | sed 's|^|    |g' ;;
                b|ba|bal|bala|balan|balanc|balance)                     echo "Balance $*" && echo && balance   "$@" | sed 's|^|    |g' ;;
                t|ti|tic|tick|ticke|ticker)                             echo "Ticker $*"  && echo && ticker    "$@" | sed 's|^|    |g' ;;
                m|ma|mar|mark|marke|market|markets)                     echo "Markets $*" && echo && markets   "$@" | sed 's|^|    |g' ;;
                o|or|ord|orde|order|orders)                             echo "Orders $*"  && echo && orders    "$@" | sed 's|^|    |g' ;;
                B|BU|BUY)                                               echo "BUY $*"     && echo && BUY       "$@" | sed 's|^|    |g' ;;
                S|SE|SEL|SELL)                                          echo "SELL $*"    && echo && SELL      "$@" | sed 's|^|    |g' ;;
                c|ca|can|canc|cance|cancel)                             echo "Cancel $*"  && echo && cancel    "$@" | sed 's|^|    |g' ;;
                q|qu|qui|quit)                                                                       quit    ;;
                h|he|hel|help)                                                                       help    ;;
                *)                        help && printf '\nInvalid command "%s"\n' "$action" ;;
        esac
}


if initialize ; then

        if [ -n "$1" ]; then
                execute "$@"
        else
                help
                while true ; do
                        printf 'Execute: ' && read -r action && clear
                        # shellcheck disable=SC2086
                        (execute $action) ; code=$?
                        [ "$code" -eq 255 ] && exit
                        [ "$code" -gt   0 ] && echo "Error $code"
                done
        fi
else code=$?
fi

return "$code"
