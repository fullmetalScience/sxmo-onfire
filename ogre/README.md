# OGRE - TradeOgre Console and CLI

**OGRE** is a simple command-line- and console interface to the TradeOgre trading platform.
**OGRE** is a simple terminal interface, both console and CLI, optimized for swing-trading XMR<->BTC on TradeOgre.

It spares you the hassle of having to log in via a browser just to update orders.

There is no affiliation with the platform.


## Installation

1. Get **OGRE** by cloning [sxmo-onfire](../).
2. Install dependencies listed by this command:
        _common/assert_environment.sh ogre/ogre.sh


## Usage

To start the **interactive menu**, run **OGRE** without providing a command name as parameter:
    API_KEY=<key> API_SECRET=<secret> ./ogre.sh

To execute a specific **command directly**, provide it as first parameter, adding arguments as needed (`./ogre.sh <command> [<params>...]`). Example:
    API_KEY=<key> API_SECRET=<secret> ./ogre.sh ticker


## Features

* Instead of provding `API_KEY=` and `API_SECRET=`, a `PASSFILE` containing those on separate lines may be provided.
* If the `PASSFILE` name ends in `.gpg`, decryption is attempted. Otherwise it is considered a plain-text file. In either case, *only lines* starting with `API_KEY` or `API_SECRET` of the provided file are considered.
* **Copies of order data** in JSON format are stored locally for later reference. Cancelling an order also removes the corresponding copy.
* To get **history-functionality**, install *rlwrap* and launch like this: `PASSFILE=<file> rlwrap -nN ./ogre.sh`. The arrow-up key will then recall previously issued commands.
